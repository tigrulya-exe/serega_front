import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import AuthenticatedRoute from "./AuthenticatedRoute";
import UnauthenticatedRoute from "./UnauthenticatedRoute";

import {Switch,} from "react-router-dom";
import AreasCrudTable from "../components/tables/crud/AreasCrudTable";
import ProductTestingCrudTable from "../components/tables/crud/ProductTestingCrudTable";
import BrigadesCrudTable from "../components/tables/crud/BrigadesCrudTable";
import EngineersCrudTable from "../components/tables/crud/EngineersCrudTable";
import LabEquipmentCrudTable from "../components/tables/crud/LabEquipmentCrudTable";
import LabsCrudTable from "../components/tables/crud/LabsCrudTable";
import MastersCrudTable from "../components/tables/crud/MastersCrudTable";
import ProductsCrudTable from "../components/tables/crud/ProductsCrudTable";
import TestersCrudTable from "../components/tables/crud/TestersCrudTable";
import WorkersCrudTable from "../components/tables/crud/WorkersCrudTable";
import WorkshopsCrudTable from "../components/tables/crud/WorkshopsCrudTable";
import Login from "../components/forms/Login";
import RegistrationForm from "../components/forms/RegistrationForm";
import PasswordRestoreForm from "../components/forms/PasswordRestoreForm";
import AssemblyCrudTable from "../components/tables/crud/AssemblyCrudTable";
import QueryForm from "../components/QueryForm";
import AreasByWorkshop from "../components/queries/AreasByWorkshop";
import BrigadesByAssembly from "../components/queries/BrigadesByAssembly";
import BrigadesByAreaOrWorkshop from "../components/queries/BrigadesByAreaOrWorkshop";
import AssemblyByProduct from "../components/queries/AssemblyByProduct";
import MastersByAreaOrWorkshop from "../components/queries/MastersByAreaOrWorkshop";
import LabsByProduct from "../components/queries/LabsByProduct";
import ProductsByTypeAndWorkshop from "../components/queries/ProductsByTypeAndWorkshop";
import LabEquipmentByLabAndProduct from "../components/queries/LabEquipmentByLabAndProduct";
import ProductsByLabAndType from "../components/queries/ProductsByLabAndType";
import ProductsByWorkshopOrAreaAndType from "../components/queries/ProductsByWorkshopOrAreaAndType";
import EmployeesByWorkShop from "../components/queries/EmployeesByWorkShop";
import TestersByLabAndProductTesting from "../components/queries/TestersByLabAndProductTesting";

export default function RouteSwitch(props) {
    return (
        <Switch>
            <AuthenticatedRoute
                component={<AreasByWorkshop/>}
                path="/areas-by-workshop">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<BrigadesByAssembly/>}
                path="/brigades-by-assembly">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<BrigadesByAreaOrWorkshop/>}
                path="/brigades-by-area-or-workshop">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<AssemblyByProduct/>}
                path="/assembly-by-product">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<LabsByProduct/>}
                path="/labs-by-product">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<MastersByAreaOrWorkshop/>}
                path="/masters-by-area-or-workshop">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ProductsByTypeAndWorkshop/>}
                path="/products-by-type-and-workshop">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<EmployeesByWorkShop/>}
                path="/employees-by-workshop/:name">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<TestersByLabAndProductTesting/>}
                path="/testers-by-lab-and-product-testing">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ProductsByLabAndType/>}
                path="/products-by-lab-and-type">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ProductsByWorkshopOrAreaAndType/>}
                path="/products-by-workshop-or-area-and-type">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<LabEquipmentByLabAndProduct/>}
                path="/lab-equipment-by-lab-and-product">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<AreasCrudTable/>}
                path="/areas">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<AssemblyCrudTable/>}
                path="/assembly">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<BrigadesCrudTable/>}
                path="/brigades">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<EngineersCrudTable/>}
                path="/engineers">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<LabEquipmentCrudTable/>}
                path="/labEquipment">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<LabsCrudTable/>}
                path="/labs">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<MastersCrudTable/>}
                path="/masters">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ProductsCrudTable/>}
                path="/products">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ProductTestingCrudTable/>}
                path="/productTesting">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<TestersCrudTable/>}
                path="/testers">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<WorkersCrudTable/>}
                path="/workers">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<WorkshopsCrudTable/>}
                path="/workshops">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<QueryForm/>}
                path="/query">
            </AuthenticatedRoute>
            <UnauthenticatedRoute
                component={<Login/>}
                path="/login">
            </UnauthenticatedRoute>
            <UnauthenticatedRoute
                component={<RegistrationForm/>}
                path="/sign-up">
            </UnauthenticatedRoute>
            <UnauthenticatedRoute
                component={<PasswordRestoreForm/>}
                path="/restore">
            </UnauthenticatedRoute>
        </Switch>
    );
}

