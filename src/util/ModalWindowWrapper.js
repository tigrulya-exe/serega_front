import React from 'react';
import BlockWithModal from "../components/modals/BlockWithModal";

export default function ModalWindowWrapper(props) {
    return (
        <BlockWithModal>
            {(modalProps) => {
                const Component = props.component;
                return (<Component
                    {...props}
                    {...modalProps}
                />)
            }}
        </BlockWithModal>
    )
}