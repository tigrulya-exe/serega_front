export default class TokenStorage {
    constructor(listeners) {
        this.isAuthorized = localStorage.getItem("jwt");
        this.listeners = listeners;
    }

    notifyListeners = () => {
        for (const listener of this.listeners) {
            listener(this.isAuthorized)
        }
    };

    setTokens = (tokens) => {
        localStorage.setItem('jwt', tokens.token);
    };

    login = (tokens) => {
        this.setTokens(tokens);
        this.isAuthorized = true;
        this.notifyListeners()
    };

    logout = () => {
        localStorage.removeItem('jwt');
        this.isAuthorized = false;
        this.notifyListeners()
    };
}