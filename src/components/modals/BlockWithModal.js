import React from 'react';
import ModalWindow from "./Modal";

export default class BlockWithModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalMessage: '',
            modalTitle: '',
            showModal: false,
        };
    }

    onModalClose = () => {
        this.setState({
            showModal: false
        })
    };

    showModal = (text, title) => {
        this.setState({
            modalTitle: title,
            modalMessage: text,
            showModal: true
        });
    };

    render() {
        return (
            <>
                <ModalWindow
                    show={this.state.showModal}
                    title={this.state.modalTitle}
                    message={this.state.modalMessage}
                    onModalClose={this.onModalClose}
                />
                {this.props.children({
                    showModal: this.showModal,
                })}
            </>
        )
    }


}