import React from 'react';
import MaterialTable from 'material-table';
import {AXIOS} from '../../util/AxiosConfig'

export default class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            params: this.props.params,
            selectedRows: []
        };

        this.tableRef = this.props.tableRef || React.createRef();
        this.props.setRefresh && this.props.setRefresh(this.onRefresh);
    }

    showError = (text) => this.props.showModal(text, 'Error!');

    getErrorFromReason = (reason) => reason.response?.data?.message;

    resolveResults = (resolve, response) => {
        const newData = response.data.content;
        resolve({
            data: newData,
            page: response.data.pageNumber,
            totalCount: response.data.totalElements,
        })
    };

    getAllEntities = (query) => {
        console.log(query)

        console.log(this.getQueryParams(query))
        return AXIOS.get(this.props.url,
            {
                params: this.getQueryParams(query)
            });
    };

    getQueryParams = (query) => ({
        ...this.props.params,
        page: query.page,
        pageSize: query.pageSize,
        orderBy: query?.orderBy?.sortField || query?.orderBy?.field,
        // order: query.orderDirection.length === 0 ? null : (query.orderDirection === 'asc' ? 'ASCENDING' : 'DESCENDING')
        order: query.orderDirection && (query.orderDirection === 'asc' ? 'ASCENDING' : 'DESCENDING')
    });

    onRefresh = () => {
        this.props.onRefresh && this.props.onRefresh();
        return this.tableRef.current?.onQueryChange()
    };

    onDelete = (row) => {
        const promise = this.props.onDelete(row);
        this.tableRef.current.onQueryChange();
        return promise;
    };

    render() {
        return (
            <MaterialTable
                tableRef={this.tableRef}
                title={this.props.tableName}
                columns={this.props.columns}
                data={query =>
                    new Promise((resolve, reject) => {
                        this.getAllEntities(query)
                            .then(response => this.resolveResults(resolve, response))
                            .catch((reason) => {
                                this.showError(`Error refreshing entities: ${this.getErrorFromReason(reason)}`);
                                resolve({
                                    data: [],
                                    page: 0,
                                    totalCount: 0
                                })
                            })
                    })
                }
                options={{
                    actionsColumnIndex: -1,
                    search: false,
                    pageSize: 5,
                    pageSizeOptions: [],
                    selection: this.props.onSelectSubmit,
                    selectionProps: this.props.selectionProps
                }}
                onSelectionChange={this.props.onSelectSubmit}
                editable={{
                    onRowDelete: this.props?.onRowDelete,
                    onRowAdd: this.props?.onRowAdd,
                    onRowUpdate: this.props?.onRowUpdate,
                }}
                actions={[
                    {
                        icon: 'refresh',
                        tooltip: 'Refresh Data',
                        isFreeAction: true,
                        onClick: this.onRefresh,
                    },
                    this.props.action
                ]}
                detailPanel={this.props.detailPanel}
                onRowClick={this.props.onRowClick}
            />
        )
    }
}