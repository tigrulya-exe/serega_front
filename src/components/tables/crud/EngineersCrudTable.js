import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import CrudTable from "./CrudTable";
import columns from "../../columns/TableColumns";

export default class EngineersCrudTable extends React.Component {
    render() {
        return (
            <ModalWindowWrapper
                component={CrudTable}
                columns={columns.get('engineers')}
                url='/engineers'
                tableName='Engineers'
            />
        )
    }
}