import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import CrudTable from "./CrudTable";
import columns from "../../columns/TableColumns";

export default class MastersCrudTable extends React.Component {
    render() {
        return (
            <ModalWindowWrapper
                component={CrudTable}
                columns={columns.get('masters')}
                url='/masters'
                tableName='Masters'
            />
        )
    }
}