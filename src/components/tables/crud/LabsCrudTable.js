import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import CrudTable from "./CrudTable";
import columns from "../../columns/TableColumns";

export default class LabsCrudTable extends React.Component {
    render() {
        return (
            <ModalWindowWrapper
                component={CrudTable}
                columns={columns.get('labs')}
                url='/labs'
                tableName='Labs'
            />
        )
    }
}