import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import CrudTable from "./CrudTable";
import columns from "../../columns/TableColumns";

export default class AssemblyCrudTable extends React.Component {
    render() {
        return (
            <ModalWindowWrapper
                component={CrudTable}
                columns={columns.get('assembly')}
                url='/assembly'
                tableName='Assembly'
            />
        )
    }
}