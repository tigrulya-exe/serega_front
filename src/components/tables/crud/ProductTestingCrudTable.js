import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import CrudTable from "./CrudTable";
import columns from "../../columns/TableColumns";

export default class ProductTestingCrudTable extends React.Component {
    render() {
        return (
            <ModalWindowWrapper
                component={CrudTable}
                columns={columns.get('productTesting')}
                url='/productTesting'
                tableName='ProductTesting'
            />
        )
    }
}