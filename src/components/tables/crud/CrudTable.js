import React from 'react';
import {AXIOS} from '../../../util/AxiosConfig'
import Table from "../Table";

export default class CrudTable extends React.Component {
    constructor(props) {
        super(props);
        this.tableRef = React.createRef();
    }

    showSuccess = (text) => this.props.showModal(text, 'Success!');

    showError = (text) => this.props.showModal(text, 'Error!');

    getErrorFromReason = (reason) => reason.response?.data?.message;

    addEntity = newData =>
        new Promise(resolve => {
            AXIOS.post(`${this.props.url}`, newData)
                .then(() => {
                    this.showSuccess('Entity was added!');
                    this.tableRef.current.onQueryChange();
                })
                .catch((reason) =>
                    this.showError(`Error adding entity: ${this.getErrorFromReason(reason)}`)
                );
            resolve()
        });

    updateEntity = (newData, oldData) =>
        new Promise((resolve, reject) => {
            AXIOS.put(`${this.props.url}`, newData)
                .then(() => {
                    this.showSuccess(`Entity was updated`);
                    this.tableRef.current.onQueryChange();
                })
                .catch((reason) =>
                    this.showError(`Error updating entity: ${this.getErrorFromReason(reason)}`)
                );

            resolve()
        });

    deleteEntity = oldData =>
        new Promise((resolve, reject) => {
            AXIOS.delete(`${this.props.url}/${oldData.id}`)
                .then(() => {
                    this.showSuccess(`Entity was deleted`);
                    this.tableRef.current.onQueryChange();
                })
                .catch((reason) =>
                    this.showError(`Error deleting entity: ${this.getErrorFromReason(reason)}`)
                );

            resolve()
        });

    render() {
        return (
            <Table
                {...this.props}
                tableRef={this.tableRef}
                onRowDelete={this.deleteEntity}
                onRowAdd={this.addEntity}
                onRowUpdate={this.updateEntity}
            />
        )
    }
}