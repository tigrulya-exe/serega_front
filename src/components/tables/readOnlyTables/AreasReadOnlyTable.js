import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import Table from "../Table";
import columns from "../../columns/TableColumns";
import SingleSelectTable from "../SingleSelectTable";

export default function AreasReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('areas')}
            url={ props.url || '/areas' }
            tableName='Areas'
        />
    )
}