import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import Table from "../Table";
import columns from "../../columns/TableColumns";
import SingleSelectTable from "../SingleSelectTable";

export default function WorkersReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('workers')}
            url={ props.url || '/workers' }
            tableName='Workers'
        />
    )
}