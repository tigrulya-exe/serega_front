import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import columns from "../../columns/TableColumns";
import MultiSelectTable from "../MultiSelectTable";

export default function ProductsMultiSelectTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={MultiSelectTable}
            columns={columns.get('products')}
            url={props.url || '/products'}
            tableName='Products'
        />
    )
}