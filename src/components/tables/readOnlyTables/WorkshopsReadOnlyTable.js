import React from 'react';
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import columns from "../../columns/TableColumns";
import Table from "../Table";
import SingleSelectTable from "../SingleSelectTable";

export default function WorkshopsReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('workshops')}
            url={props.url || '/workshops'}
            tableName='Workshops'
        />
    )
}