import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import Table from "../Table";
import columns from "../../columns/TableColumns";
import SingleSelectTable from "../SingleSelectTable";

export default function EngineersReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('engineers')}
            url={ props.url || '/engineers' }
            tableName='Engineers'
        />
    )
}