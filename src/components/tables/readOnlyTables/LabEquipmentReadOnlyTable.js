import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import Table from "../Table";
import columns from "../../columns/TableColumns";
import SingleSelectTable from "../SingleSelectTable";

export default function LabEquipmentReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('labEquipment')}
            url={ props.url || '/labEquipment' }
            tableName='Lab equipment'
        />
    )
}