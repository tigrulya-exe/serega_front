import React from "react";
import ModalWindowWrapper from "../../../util/ModalWindowWrapper";
import Table from "../Table";
import columns from "../../columns/TableColumns";
import SingleSelectTable from "../SingleSelectTable";

export default function AssemblyReadOnlyTable(props) {
    return (
        <ModalWindowWrapper
            {...props}
            component={props.onSelectSubmit ? SingleSelectTable : Table}
            columns={columns.get('assembly')}
            url={ props.url || '/assembly' }
            tableName='Assembly'
        />
    )
}