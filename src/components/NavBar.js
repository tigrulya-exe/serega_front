import React from 'react';
import {Button, Form, Nav, Navbar, NavDropdown} from 'react-bootstrap';

import {Link} from "react-router-dom";
import {AuthContext} from "../context/AuthContextProvider";
import history from "../routing/History";

export default class NavBar extends React.Component {
    static contextType = AuthContext;

    onSignIn = () => {
        history.push('/login');
    };

    onLogout = () => {
        history.push('/login');
        this.context.logout();
    };

    onSignUp = () => {
        history.push('/sign-up');
    };

    render() {
        return (
            <Navbar bg="dark" variant="dark" expand="lg">
                <Navbar.Brand><Link style={{textDecoration: 'none', color: 'white'}} to="/">Aircraft</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavDropdown title="Tables" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to="/areas">Areas</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/workshops">Workshops</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/brigades">Brigades</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/engineers">Engineers</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/masters">Masters</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/testers">Testers</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/workers">Workers</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/labEquipment">LabEquipment</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/labs">Labs</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/productTesting">ProductTesting</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/assembly">Assembly</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/products">Products</NavDropdown.Item>

                        </NavDropdown>
                        <NavDropdown title="Queries" id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to="/areas-by-workshop">Areas by workshop</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/brigades-by-area-or-workshop">Brigades by area or workshop</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/brigades-by-assembly">Brigades by assembly</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/labs-by-product">Labs by product</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/assembly-by-product">Assembly by product</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/masters-by-area-or-workshop">Masters by area or workshop</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/products-by-type-and-workshop">Products by type and workshop</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/lab-equipment-by-lab-and-product">Lab equipment by lab and product</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/products-by-lab-and-type">Products by lab and type</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/products-by-workshop-or-area-and-type">Products by workshop or area and type</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/employees-by-workshop/workers">Employees by workshop</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/testers-by-lab-and-product-testing">Testers by lab and product testing</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Nav.Link as={Link} style={{textDecoration: 'none', color: 'white'}}
                                    to="/query">Native query</Nav.Link>
                    {
                        this.context.isAuthorized
                            ? <Form inline>
                                <Button variant="danger" onClick={this.onLogout}>Logout</Button>
                            </Form>
                            : <Form inline>
                                <Button variant="light" onClick={this.onSignIn}>Sign In</Button>
                                <Button variant="primary" onClick={this.onSignUp}>Sign Up</Button>
                            </Form>
                    }

                </Navbar.Collapse>
            </Navbar>
        )
    }
}