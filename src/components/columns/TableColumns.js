const columns = new Map();

columns.set('areas', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Work shop id', field: 'workshopId', type: 'numeric'},
    {title: 'Serial Number', field: 'serialNumber', type: 'numeric'},
    {title: 'Area superior id', field: 'areaSuperiorId', type: 'numeric'},
]);

columns.set('assembly', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Product id', field: 'productId', type: 'numeric'},
    {title: 'Area id', field: 'areaId', type: 'numeric'},
    {title: 'Brigade id', field: 'brigadeId', type: 'numeric'},
    {title: 'Start date', field: 'startDate', type: 'datetime'},
    {title: 'End date', field: 'endDate', type: 'datetime'},
]);

columns.set('brigades', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Area id', field: 'areaId', type: 'numeric'},
    {title: 'Brigade leader id', field: 'brigadeLeaderId', type: 'numeric'}
]);

columns.set('engineers', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
    {title: 'Surname', field: 'surname'},
    {title: 'Middle name', field: 'middleName'},
    {title: 'Profession', field: 'profession'},
    {title: 'Master id', field: 'masterId', type: 'numeric'},
]);

columns.set('labEquipment', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Lab id', field: 'labId', type: 'numeric'},
]);

columns.set('labs', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
]);

columns.set('masters', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
    {title: 'Surname', field: 'surname'},
    {title: 'Middle name', field: 'middleName'},
    {title: 'Profession', field: 'profession'},
    {title: 'Area id', field: 'areaId', type: 'numeric'},
]);

columns.set('products', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Type', field: 'type'},
    {title: 'Product condition', field: 'productCondition'},
    {title: 'Attributes', field: 'attributes'},
]);

columns.set('productTesting', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Product id', field: 'productId', type: 'numeric'},
    {title: 'Tester id', field: 'testerId', type: 'numeric'},
    {title: 'Lab equipment id', field: 'labEquipmentId', type: 'numeric'},
    {title: 'Start date', field: 'startDate', type: 'datetime'},
    {title: 'End date', field: 'endDate', type: 'datetime'},
]);

columns.set('testers', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
    {title: 'Surname', field: 'surname'},
    {title: 'Middle name', field: 'middleName'},
]);

columns.set('workers', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
    {title: 'Surname', field: 'surname'},
    {title: 'Middle name', field: 'middleName'},
    {title: 'Profession', field: 'profession'},
    {title: 'Brigade id', field: 'brigadeId', type: 'numeric'},
]);

columns.set('workshops', [
    {title: 'Id', field: 'id', type: 'numeric', editable: 'never'},
    {title: 'Name', field: 'name'},
    {title: 'Workshop superior id', field: 'workshopSuperiorId', type: 'numeric'},
]);

export default columns;