import React from 'react';
import {Form} from "react-bootstrap";
import BrigadesReadOnlyTable from "../tables/readOnlyTables/BrigadesReadOnlyTable";
import AreasReadOnlyTable from "../tables/readOnlyTables/AreasReadOnlyTable";
import WorkshopsReadOnlyTable from "../tables/readOnlyTables/WorkshopsReadOnlyTable";

export default class BrigadesByAreaOrWorkshop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areaId: null,
            workshopId: null
        }
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    isReady = () => this.state.areaId || this.state.workshopId;

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="AreaSelectForm">
                        <AreasReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'areaId')}
                        />
                        <br/>
                        <Form.Label>{this.state.areaId && `Selected area id: ${this.state.areaId}`}</Form.Label>
                    </Form.Group>
                    <Form.Group controlId="WorkshopSelectForm">
                        <WorkshopsReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'workshopId')}
                        />
                        <br/>
                        <Form.Label>{this.state.workshopId && `Selected workshopId id: ${this.state.workshopId}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.isReady() && <BrigadesReadOnlyTable
                    url='/brigades/by-area-or-workshop'
                    params={this.state}
                />}
            </>
        )
    }
}