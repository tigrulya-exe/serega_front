import React from 'react';
import {Form} from "react-bootstrap";
import WorkshopsReadOnlyTable from "../tables/readOnlyTables/WorkshopsReadOnlyTable";
import EngineersReadOnlyTable from "../tables/readOnlyTables/EngineersReadOnlyTable";

export default class EngineersByWorkshop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workshopId: null,
        }
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="AreaSelectForm">
                        <WorkshopsReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'workshopId')}
                        />
                        <br/>
                        <Form.Label>{this.state.workshopId && `Selected workshop id: ${this.state.workshopId}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.state.workshopId && <EngineersReadOnlyTable
                    url='/engineers/by-workshop'
                    params={this.state}
                />}
            </>
        )
    }
}