import React from 'react';
import {Col, Form} from "react-bootstrap";
import LabsReadOnlyTable from "../tables/readOnlyTables/LabsReadOnlyTable";
import ProductsReadOnlyTable from "../tables/readOnlyTables/ProductsReadOnlyTable";
import {DateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import LabEquipmentReadOnlyTable from "../tables/readOnlyTables/LabEquipmentReadOnlyTable";

export default class LabEquipmentByLabAndProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            labId: null,
            type: null,
            productId: null,
            startDate: null,
            endDate: null
        };
    }

    onMaxChange = date => {
        this.setState({endDate: date})
    };

    onMinChange = date => {
        this.setState({startDate: date})
    };

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    onFormChange = (event) => {
        this.setState({[event.target.id]: event.target.value});
    };

    isReady = () => (this.state.startDate && this.state.endDate) || this.state.type?.length;

    render() {
        return (
            <>
                <br/>
                <Form onChange={this.onFormChange} onSubmit={e => e.preventDefault()}>
                    <Form.Group controlId="type">
                        <Form.Label>Product type</Form.Label>
                        <Form.Control/>
                    </Form.Group>
                </Form>
                <br/>

                <Form.Row>
                    <Form.Group as={Col} controlId="startDate">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DateTimePicker
                                value={this.state.startDate}
                                onChange={this.onMinChange}
                                label="Min date"
                                showTodayButton
                                clearable
                            />
                        </MuiPickersUtilsProvider>
                    </Form.Group>
                    <Form.Group as={Col} controlId="endDate">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DateTimePicker
                                value={this.state.endDate}
                                onChange={this.onMaxChange}
                                label="Max date"
                                showTodayButton
                                clearable
                            />
                        </MuiPickersUtilsProvider>
                    </Form.Group>
                </Form.Row>

                <LabsReadOnlyTable
                    onSelectSubmit={(data) => this.onTableSubmit(data, 'labId')}
                />
                <br/>
                <Form.Label>{this.state.labId && `Selected lab id: ${this.state.labId}`}</Form.Label>
                <br/>

                <ProductsReadOnlyTable
                    onSelectSubmit={(data) => this.onTableSubmit(data, 'productId')}
                />
                <br/>
                <Form.Label>{this.state.productId && `Selected product id: ${this.state.productId}`}</Form.Label>
                <br/>

                {this.isReady() && <LabEquipmentReadOnlyTable
                    url='/labEquipment/by-lab-and-product-or-product-type-and-product-testing-date'
                    params={this.state}
                />}
            </>
        )
    }
}