import React from 'react';
import {Col, Form} from "react-bootstrap";
import AreasReadOnlyTable from "../tables/readOnlyTables/AreasReadOnlyTable";
import {DateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import TestersReadOnlyTable from "../tables/readOnlyTables/TestersReadOnlyTable";
import ProductsMultiSelectTable from "../tables/readOnlyTables/ProductsMultiSelectTable";
import LabsReadOnlyTable from "../tables/readOnlyTables/LabsReadOnlyTable";

export default class TestersByLabAndProductTesting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            labId: null,
            startDate: null,
            endDate: null,
            productIds: null
        };
    }

    onMaxChange = date => {
        this.setState({endDate: date})
    };

    onMinChange = date => {
        this.setState({startDate: date})
    };

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    isReady = () => (this.state.startDate && this.state.endDate) && this.state.productIds && this.state.labId;

    arrayToParams = (array) => {
        return array.length ? array.reduce((f, s) => `${f},${s}`) : ''
    };

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row>
                        <Form.Group as={Col} controlId="startDate">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DateTimePicker
                                    value={this.state.startDate}
                                    onChange={this.onMinChange}
                                    label="Min date"
                                    showTodayButton
                                    clearable
                                />
                            </MuiPickersUtilsProvider>
                        </Form.Group>
                        <Form.Group as={Col} controlId="endDate">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DateTimePicker
                                    value={this.state.endDate}
                                    onChange={this.onMaxChange}
                                    label="Max date"
                                    showTodayButton
                                    clearable
                                />
                            </MuiPickersUtilsProvider>
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId="AreaSelectForm">
                        <LabsReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'labId')}
                        />
                        <br/>
                        <Form.Label>{this.state.labId && `Selected lab id: ${this.state.labId}`}</Form.Label>
                    </Form.Group>
                    <Form.Group controlId="AreaSelectForm">
                        <ProductsMultiSelectTable
                            onSelectSubmit={(data) => this.setState({productIds: this.arrayToParams(data)})}
                        />
                        <br/>
                        <Form.Label>{this.state.productIds && `Selected product ids: ${this.state.productIds}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.isReady() && <TestersReadOnlyTable
                    url='/testers/by-lab-and-product-and-product-testing-date'
                    params={this.state}
                />}
            </>
        )
    }
}