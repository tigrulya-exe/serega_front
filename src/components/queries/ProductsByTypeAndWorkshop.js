import React from 'react';
import {Form} from "react-bootstrap";
import WorkshopsReadOnlyTable from "../tables/readOnlyTables/WorkshopsReadOnlyTable";
import ProductsReadOnlyTable from "../tables/readOnlyTables/ProductsReadOnlyTable";

export default class ProductsByTypeAndWorkshop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workshopId: null,
            type: null,
        };
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    onFormChange = (event) => {
        this.setState({[event.target.id]: event.target.value});
    };

    // isReady = () => this.state.workshopId || this.state.type?.length;

    render() {
        return (
            <>
                <br/>
                <Form onChange={this.onFormChange} onSubmit={e => e.preventDefault()}>
                    <Form.Group controlId="type">
                        <Form.Label>Product type</Form.Label>
                        <Form.Control/>
                    </Form.Group>
                </Form>
                <br/>
                <WorkshopsReadOnlyTable
                    onSelectSubmit={(data) => this.onTableSubmit(data, 'workshopId')}
                />
                <br/>
                <Form.Label>{this.state.workshopId && `Selected workshop id: ${this.state.workshopId}`}</Form.Label>
                <br/>

                <ProductsReadOnlyTable
                    url='/products/by-type-and-workshop'
                    params={this.state}
                />
            </>
        )
    }
}