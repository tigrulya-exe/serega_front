import React from 'react';
import {Form} from "react-bootstrap";
import WorkshopsReadOnlyTable from "../tables/readOnlyTables/WorkshopsReadOnlyTable";
import WorkersReadOnlyTable from "../tables/readOnlyTables/WorkersReadOnlyTable";

export default class WorkersByWorkshop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workshopId: null,
        }
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="AreaSelectForm">
                        <WorkshopsReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'workshopId')}
                        />
                        <br/>
                        <Form.Label>{this.state.workshopId && `Selected workshop id: ${this.state.workshopId}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.state.workshopId && <WorkersReadOnlyTable
                    url='/workers/by-workshop'
                    params={this.state}
                />}
            </>
        )
    }
}