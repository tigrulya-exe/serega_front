import React from 'react';
import {Form} from "react-bootstrap";
import ProductsReadOnlyTable from "../tables/readOnlyTables/ProductsReadOnlyTable";
import AssemblyReadOnlyTable from "../tables/readOnlyTables/AssemblyReadOnlyTable";

export default class AssemblyByProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productId: null,
        }
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="AreaSelectForm">
                        <ProductsReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'productId')}
                        />
                        <br/>
                        <Form.Label>{this.state.productId && `Selected product id: ${this.state.productId}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.state.productId && <AssemblyReadOnlyTable
                    url='/assembly/by-product'
                    params={this.state}
                />}
            </>
        )
    }
}