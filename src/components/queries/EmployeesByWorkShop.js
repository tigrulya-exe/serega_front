import React from 'react';
import Nav from "react-bootstrap/Nav";
import {Link, useParams} from "react-router-dom";
import WorkersByWorkshop from "./WorkersByWorkshop";
import EngineersByWorkshop from "./EngineersByWorkshop";

const pageMap = new Map();

pageMap.set('workers', <WorkersByWorkshop/>);
pageMap.set('engineers', <EngineersByWorkshop/>);

export default function EmployeesByWorkShop(props) {
    const {name} = useParams();
    return (
        <>
            <Nav variant="tabs" defaultActiveKey={name}>
                <Nav.Item>
                    <Nav.Link eventKey="workers"><Link to="/employees-by-workshop/workers">Workers</Link></Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="engineers"><Link to="/employees-by-workshop/engineers">Engineers</Link></Nav.Link>
                </Nav.Item>
            </Nav>
            {pageMap.get(name)}
        </>
    )

}