import React from 'react';
import {Form} from "react-bootstrap";
import AssemblyReadOnlyTable from "../tables/readOnlyTables/AssemblyReadOnlyTable";
import BrigadesReadOnlyTable from "../tables/readOnlyTables/BrigadesReadOnlyTable";

export default class BrigadesByAssembly extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            assemblyId: null,
        }
    }

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    render() {
        return (
            <>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="AreaSelectForm">
                        <AssemblyReadOnlyTable
                            onSelectSubmit={(data) => this.onTableSubmit(data, 'assemblyId')}
                        />
                        <br/>
                        <Form.Label>{this.state.assemblyId && `Selected assembly id: ${this.state.assemblyId}`}</Form.Label>
                    </Form.Group>
                </Form>
                {this.state.assemblyId && <BrigadesReadOnlyTable
                    url='/brigades/by-assembly'
                    params={this.state}
                />}
            </>
        )
    }
}