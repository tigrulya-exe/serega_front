import React from 'react';
import {Col, Form} from "react-bootstrap";
import {DateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import WorkshopsReadOnlyTable from "../tables/readOnlyTables/WorkshopsReadOnlyTable";
import ProductsReadOnlyTable from "../tables/readOnlyTables/ProductsReadOnlyTable";
import AreasReadOnlyTable from "../tables/readOnlyTables/AreasReadOnlyTable";

export default class ProductsByWorkshopOrAreaAndType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workshopId: null,
            areaId: null,
            type: null,
            currentDate: null,
        };
    }

    onCurrentDateChange = date => {
        this.setState({currentDate: date})
    };

    onTableSubmit = (data, propName) => {
        this.setState({[propName]: data})
    };

    onFormChange = (event) => {
        this.setState({[event.target.id]: event.target.value});
    };

    isReady = () => this.state.currentDate;

    render() {
        return (
            <>
                <br/>
                <Form onChange={this.onFormChange} onSubmit={e => e.preventDefault()}>
                    <Form.Row>
                        <Col sm={10}>
                            <Form.Group controlId="type">
                                <Form.Label>Product type</Form.Label>
                                <Form.Control/>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group as={Col} controlId="currentDate">
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DateTimePicker
                                        value={this.state.currentDate}
                                        onChange={this.onCurrentDateChange}
                                        label="Date"
                                        showTodayButton
                                        clearable
                                    />
                                </MuiPickersUtilsProvider>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                </Form>
                < br/>
                < AreasReadOnlyTable
                    onSelectSubmit={(data) =>
                        this.onTableSubmit(data, 'areaId')
                    }
                />
                <br/>
                < Form.Label> {this.state.areaId && `Selected area id: ${this.state.areaId}`}
                </Form.Label>
                <WorkshopsReadOnlyTable
                    onSelectSubmit={(data) => this.onTableSubmit(data, 'workshopId')}
                />
                <br/>
                <Form.Label>{this.state.workshopId && `Selected workshopId id: ${this.state.workshopId}`}</Form.Label>

                {this.isReady() && <ProductsReadOnlyTable
                    url='/products/by-type-and-area-or-workshop-at-current-date'
                    params={this.state}
                />}
            </>
        )
    }
}